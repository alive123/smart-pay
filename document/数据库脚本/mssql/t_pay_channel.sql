
CREATE TABLE [dbo].[t_pay_channel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [varchar](24) NOT NULL,
	[ChannelName] [varchar](30) NOT NULL,
	[ChannelMchId] [varchar](32) NOT NULL,
	[MchId] [varchar](30) NOT NULL,
	[State] [int] NOT NULL,
	[Param] [varchar](4096) NOT NULL,
	[Remark] [varchar](128) NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK__t_pay_ch__3214EC07060DEAE8] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_pay_channel] ADD  CONSTRAINT [DF__t_pay_cha__State__07F6335A]  DEFAULT ('1') FOR [State]
GO

ALTER TABLE [dbo].[t_pay_channel] ADD  CONSTRAINT [DF__t_pay_cha__Remar__08EA5793]  DEFAULT (NULL) FOR [Remark]
GO

ALTER TABLE [dbo].[t_pay_channel] ADD  CONSTRAINT [DF__t_pay_cha__Creat__09DE7BCC]  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[t_pay_channel] ADD  CONSTRAINT [DF__t_pay_cha__Updat__0AD2A005]  DEFAULT ('0000-00-00 00:00:00') FOR [UpdateTime]
GO


