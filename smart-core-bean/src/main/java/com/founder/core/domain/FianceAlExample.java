package com.founder.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FianceAlExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public FianceAlExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMchidIsNull() {
            addCriterion("mchid is null");
            return (Criteria) this;
        }

        public Criteria andMchidIsNotNull() {
            addCriterion("mchid is not null");
            return (Criteria) this;
        }

        public Criteria andMchidEqualTo(String value) {
            addCriterion("mchid =", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotEqualTo(String value) {
            addCriterion("mchid <>", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidGreaterThan(String value) {
            addCriterion("mchid >", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidGreaterThanOrEqualTo(String value) {
            addCriterion("mchid >=", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLessThan(String value) {
            addCriterion("mchid <", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLessThanOrEqualTo(String value) {
            addCriterion("mchid <=", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLike(String value) {
            addCriterion("mchid like", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotLike(String value) {
            addCriterion("mchid not like", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidIn(List<String> values) {
            addCriterion("mchid in", values, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotIn(List<String> values) {
            addCriterion("mchid not in", values, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidBetween(String value1, String value2) {
            addCriterion("mchid between", value1, value2, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotBetween(String value1, String value2) {
            addCriterion("mchid not between", value1, value2, "mchid");
            return (Criteria) this;
        }

        public Criteria andTradetypeIsNull() {
            addCriterion("tradetype is null");
            return (Criteria) this;
        }

        public Criteria andTradetypeIsNotNull() {
            addCriterion("tradetype is not null");
            return (Criteria) this;
        }

        public Criteria andTradetypeEqualTo(String value) {
            addCriterion("tradetype =", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotEqualTo(String value) {
            addCriterion("tradetype <>", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeGreaterThan(String value) {
            addCriterion("tradetype >", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeGreaterThanOrEqualTo(String value) {
            addCriterion("tradetype >=", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLessThan(String value) {
            addCriterion("tradetype <", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLessThanOrEqualTo(String value) {
            addCriterion("tradetype <=", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLike(String value) {
            addCriterion("tradetype like", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotLike(String value) {
            addCriterion("tradetype not like", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeIn(List<String> values) {
            addCriterion("tradetype in", values, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotIn(List<String> values) {
            addCriterion("tradetype not in", values, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeBetween(String value1, String value2) {
            addCriterion("tradetype between", value1, value2, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotBetween(String value1, String value2) {
            addCriterion("tradetype not between", value1, value2, "tradetype");
            return (Criteria) this;
        }

        public Criteria andBzorderIsNull() {
            addCriterion("bzorder is null");
            return (Criteria) this;
        }

        public Criteria andBzorderIsNotNull() {
            addCriterion("bzorder is not null");
            return (Criteria) this;
        }

        public Criteria andBzorderEqualTo(String value) {
            addCriterion("bzorder =", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotEqualTo(String value) {
            addCriterion("bzorder <>", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderGreaterThan(String value) {
            addCriterion("bzorder >", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderGreaterThanOrEqualTo(String value) {
            addCriterion("bzorder >=", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLessThan(String value) {
            addCriterion("bzorder <", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLessThanOrEqualTo(String value) {
            addCriterion("bzorder <=", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLike(String value) {
            addCriterion("bzorder like", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotLike(String value) {
            addCriterion("bzorder not like", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderIn(List<String> values) {
            addCriterion("bzorder in", values, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotIn(List<String> values) {
            addCriterion("bzorder not in", values, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderBetween(String value1, String value2) {
            addCriterion("bzorder between", value1, value2, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotBetween(String value1, String value2) {
            addCriterion("bzorder not between", value1, value2, "bzorder");
            return (Criteria) this;
        }

        public Criteria andAlorderIsNull() {
            addCriterion("alorder is null");
            return (Criteria) this;
        }

        public Criteria andAlorderIsNotNull() {
            addCriterion("alorder is not null");
            return (Criteria) this;
        }

        public Criteria andAlorderEqualTo(String value) {
            addCriterion("alorder =", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderNotEqualTo(String value) {
            addCriterion("alorder <>", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderGreaterThan(String value) {
            addCriterion("alorder >", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderGreaterThanOrEqualTo(String value) {
            addCriterion("alorder >=", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderLessThan(String value) {
            addCriterion("alorder <", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderLessThanOrEqualTo(String value) {
            addCriterion("alorder <=", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderLike(String value) {
            addCriterion("alorder like", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderNotLike(String value) {
            addCriterion("alorder not like", value, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderIn(List<String> values) {
            addCriterion("alorder in", values, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderNotIn(List<String> values) {
            addCriterion("alorder not in", values, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderBetween(String value1, String value2) {
            addCriterion("alorder between", value1, value2, "alorder");
            return (Criteria) this;
        }

        public Criteria andAlorderNotBetween(String value1, String value2) {
            addCriterion("alorder not between", value1, value2, "alorder");
            return (Criteria) this;
        }

        public Criteria andTrademoneyIsNull() {
            addCriterion("trademoney is null");
            return (Criteria) this;
        }

        public Criteria andTrademoneyIsNotNull() {
            addCriterion("trademoney is not null");
            return (Criteria) this;
        }

        public Criteria andTrademoneyEqualTo(BigDecimal value) {
            addCriterion("trademoney =", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyNotEqualTo(BigDecimal value) {
            addCriterion("trademoney <>", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyGreaterThan(BigDecimal value) {
            addCriterion("trademoney >", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("trademoney >=", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyLessThan(BigDecimal value) {
            addCriterion("trademoney <", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("trademoney <=", value, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyIn(List<BigDecimal> values) {
            addCriterion("trademoney in", values, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyNotIn(List<BigDecimal> values) {
            addCriterion("trademoney not in", values, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("trademoney between", value1, value2, "trademoney");
            return (Criteria) this;
        }

        public Criteria andTrademoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("trademoney not between", value1, value2, "trademoney");
            return (Criteria) this;
        }

        public Criteria andBztitleIsNull() {
            addCriterion("bztitle is null");
            return (Criteria) this;
        }

        public Criteria andBztitleIsNotNull() {
            addCriterion("bztitle is not null");
            return (Criteria) this;
        }

        public Criteria andBztitleEqualTo(String value) {
            addCriterion("bztitle =", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleNotEqualTo(String value) {
            addCriterion("bztitle <>", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleGreaterThan(String value) {
            addCriterion("bztitle >", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleGreaterThanOrEqualTo(String value) {
            addCriterion("bztitle >=", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleLessThan(String value) {
            addCriterion("bztitle <", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleLessThanOrEqualTo(String value) {
            addCriterion("bztitle <=", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleLike(String value) {
            addCriterion("bztitle like", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleNotLike(String value) {
            addCriterion("bztitle not like", value, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleIn(List<String> values) {
            addCriterion("bztitle in", values, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleNotIn(List<String> values) {
            addCriterion("bztitle not in", values, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleBetween(String value1, String value2) {
            addCriterion("bztitle between", value1, value2, "bztitle");
            return (Criteria) this;
        }

        public Criteria andBztitleNotBetween(String value1, String value2) {
            addCriterion("bztitle not between", value1, value2, "bztitle");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(String value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(String value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(String value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(String value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(String value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLike(String value) {
            addCriterion("createtime like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotLike(String value) {
            addCriterion("createtime not like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<String> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<String> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(String value1, String value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(String value1, String value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andTradetimeIsNull() {
            addCriterion("tradetime is null");
            return (Criteria) this;
        }

        public Criteria andTradetimeIsNotNull() {
            addCriterion("tradetime is not null");
            return (Criteria) this;
        }

        public Criteria andTradetimeEqualTo(String value) {
            addCriterion("tradetime =", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeNotEqualTo(String value) {
            addCriterion("tradetime <>", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeGreaterThan(String value) {
            addCriterion("tradetime >", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeGreaterThanOrEqualTo(String value) {
            addCriterion("tradetime >=", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeLessThan(String value) {
            addCriterion("tradetime <", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeLessThanOrEqualTo(String value) {
            addCriterion("tradetime <=", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeLike(String value) {
            addCriterion("tradetime like", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeNotLike(String value) {
            addCriterion("tradetime not like", value, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeIn(List<String> values) {
            addCriterion("tradetime in", values, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeNotIn(List<String> values) {
            addCriterion("tradetime not in", values, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeBetween(String value1, String value2) {
            addCriterion("tradetime between", value1, value2, "tradetime");
            return (Criteria) this;
        }

        public Criteria andTradetimeNotBetween(String value1, String value2) {
            addCriterion("tradetime not between", value1, value2, "tradetime");
            return (Criteria) this;
        }

        public Criteria andMuchIsNull() {
            addCriterion("much is null");
            return (Criteria) this;
        }

        public Criteria andMuchIsNotNull() {
            addCriterion("much is not null");
            return (Criteria) this;
        }

        public Criteria andMuchEqualTo(String value) {
            addCriterion("much =", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchNotEqualTo(String value) {
            addCriterion("much <>", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchGreaterThan(String value) {
            addCriterion("much >", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchGreaterThanOrEqualTo(String value) {
            addCriterion("much >=", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchLessThan(String value) {
            addCriterion("much <", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchLessThanOrEqualTo(String value) {
            addCriterion("much <=", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchLike(String value) {
            addCriterion("much like", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchNotLike(String value) {
            addCriterion("much not like", value, "much");
            return (Criteria) this;
        }

        public Criteria andMuchIn(List<String> values) {
            addCriterion("much in", values, "much");
            return (Criteria) this;
        }

        public Criteria andMuchNotIn(List<String> values) {
            addCriterion("much not in", values, "much");
            return (Criteria) this;
        }

        public Criteria andMuchBetween(String value1, String value2) {
            addCriterion("much between", value1, value2, "much");
            return (Criteria) this;
        }

        public Criteria andMuchNotBetween(String value1, String value2) {
            addCriterion("much not between", value1, value2, "much");
            return (Criteria) this;
        }

        public Criteria andOperaIsNull() {
            addCriterion("opera is null");
            return (Criteria) this;
        }

        public Criteria andOperaIsNotNull() {
            addCriterion("opera is not null");
            return (Criteria) this;
        }

        public Criteria andOperaEqualTo(String value) {
            addCriterion("opera =", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaNotEqualTo(String value) {
            addCriterion("opera <>", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaGreaterThan(String value) {
            addCriterion("opera >", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaGreaterThanOrEqualTo(String value) {
            addCriterion("opera >=", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaLessThan(String value) {
            addCriterion("opera <", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaLessThanOrEqualTo(String value) {
            addCriterion("opera <=", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaLike(String value) {
            addCriterion("opera like", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaNotLike(String value) {
            addCriterion("opera not like", value, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaIn(List<String> values) {
            addCriterion("opera in", values, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaNotIn(List<String> values) {
            addCriterion("opera not in", values, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaBetween(String value1, String value2) {
            addCriterion("opera between", value1, value2, "opera");
            return (Criteria) this;
        }

        public Criteria andOperaNotBetween(String value1, String value2) {
            addCriterion("opera not between", value1, value2, "opera");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNull() {
            addCriterion("deviceid is null");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNotNull() {
            addCriterion("deviceid is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceidEqualTo(String value) {
            addCriterion("deviceid =", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotEqualTo(String value) {
            addCriterion("deviceid <>", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThan(String value) {
            addCriterion("deviceid >", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThanOrEqualTo(String value) {
            addCriterion("deviceid >=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThan(String value) {
            addCriterion("deviceid <", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThanOrEqualTo(String value) {
            addCriterion("deviceid <=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLike(String value) {
            addCriterion("deviceid like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotLike(String value) {
            addCriterion("deviceid not like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidIn(List<String> values) {
            addCriterion("deviceid in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotIn(List<String> values) {
            addCriterion("deviceid not in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidBetween(String value1, String value2) {
            addCriterion("deviceid between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotBetween(String value1, String value2) {
            addCriterion("deviceid not between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andTradeuserIsNull() {
            addCriterion("tradeuser is null");
            return (Criteria) this;
        }

        public Criteria andTradeuserIsNotNull() {
            addCriterion("tradeuser is not null");
            return (Criteria) this;
        }

        public Criteria andTradeuserEqualTo(String value) {
            addCriterion("tradeuser =", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserNotEqualTo(String value) {
            addCriterion("tradeuser <>", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserGreaterThan(String value) {
            addCriterion("tradeuser >", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserGreaterThanOrEqualTo(String value) {
            addCriterion("tradeuser >=", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserLessThan(String value) {
            addCriterion("tradeuser <", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserLessThanOrEqualTo(String value) {
            addCriterion("tradeuser <=", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserLike(String value) {
            addCriterion("tradeuser like", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserNotLike(String value) {
            addCriterion("tradeuser not like", value, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserIn(List<String> values) {
            addCriterion("tradeuser in", values, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserNotIn(List<String> values) {
            addCriterion("tradeuser not in", values, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserBetween(String value1, String value2) {
            addCriterion("tradeuser between", value1, value2, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTradeuserNotBetween(String value1, String value2) {
            addCriterion("tradeuser not between", value1, value2, "tradeuser");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNull() {
            addCriterion("totalmoney is null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNotNull() {
            addCriterion("totalmoney is not null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyEqualTo(BigDecimal value) {
            addCriterion("totalmoney =", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotEqualTo(BigDecimal value) {
            addCriterion("totalmoney <>", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThan(BigDecimal value) {
            addCriterion("totalmoney >", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("totalmoney >=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThan(BigDecimal value) {
            addCriterion("totalmoney <", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("totalmoney <=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIn(List<BigDecimal> values) {
            addCriterion("totalmoney in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotIn(List<BigDecimal> values) {
            addCriterion("totalmoney not in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalmoney between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalmoney not between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIsNull() {
            addCriterion("redpacketmoney is null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIsNotNull() {
            addCriterion("redpacketmoney is not null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney =", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney <>", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyGreaterThan(BigDecimal value) {
            addCriterion("redpacketmoney >", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney >=", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyLessThan(BigDecimal value) {
            addCriterion("redpacketmoney <", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney <=", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIn(List<BigDecimal> values) {
            addCriterion("redpacketmoney in", values, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotIn(List<BigDecimal> values) {
            addCriterion("redpacketmoney not in", values, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoney between", value1, value2, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoney not between", value1, value2, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyIsNull() {
            addCriterion("jifenmoney is null");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyIsNotNull() {
            addCriterion("jifenmoney is not null");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyEqualTo(BigDecimal value) {
            addCriterion("jifenmoney =", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyNotEqualTo(BigDecimal value) {
            addCriterion("jifenmoney <>", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyGreaterThan(BigDecimal value) {
            addCriterion("jifenmoney >", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("jifenmoney >=", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyLessThan(BigDecimal value) {
            addCriterion("jifenmoney <", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("jifenmoney <=", value, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyIn(List<BigDecimal> values) {
            addCriterion("jifenmoney in", values, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyNotIn(List<BigDecimal> values) {
            addCriterion("jifenmoney not in", values, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("jifenmoney between", value1, value2, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andJifenmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("jifenmoney not between", value1, value2, "jifenmoney");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxIsNull() {
            addCriterion("alipaytax is null");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxIsNotNull() {
            addCriterion("alipaytax is not null");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxEqualTo(BigDecimal value) {
            addCriterion("alipaytax =", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxNotEqualTo(BigDecimal value) {
            addCriterion("alipaytax <>", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxGreaterThan(BigDecimal value) {
            addCriterion("alipaytax >", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("alipaytax >=", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxLessThan(BigDecimal value) {
            addCriterion("alipaytax <", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("alipaytax <=", value, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxIn(List<BigDecimal> values) {
            addCriterion("alipaytax in", values, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxNotIn(List<BigDecimal> values) {
            addCriterion("alipaytax not in", values, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("alipaytax between", value1, value2, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andAlipaytaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("alipaytax not between", value1, value2, "alipaytax");
            return (Criteria) this;
        }

        public Criteria andTradetaxIsNull() {
            addCriterion("tradetax is null");
            return (Criteria) this;
        }

        public Criteria andTradetaxIsNotNull() {
            addCriterion("tradetax is not null");
            return (Criteria) this;
        }

        public Criteria andTradetaxEqualTo(BigDecimal value) {
            addCriterion("tradetax =", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxNotEqualTo(BigDecimal value) {
            addCriterion("tradetax <>", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxGreaterThan(BigDecimal value) {
            addCriterion("tradetax >", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("tradetax >=", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxLessThan(BigDecimal value) {
            addCriterion("tradetax <", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("tradetax <=", value, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxIn(List<BigDecimal> values) {
            addCriterion("tradetax in", values, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxNotIn(List<BigDecimal> values) {
            addCriterion("tradetax not in", values, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tradetax between", value1, value2, "tradetax");
            return (Criteria) this;
        }

        public Criteria andTradetaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tradetax not between", value1, value2, "tradetax");
            return (Criteria) this;
        }

        public Criteria andCancletaxIsNull() {
            addCriterion("cancletax is null");
            return (Criteria) this;
        }

        public Criteria andCancletaxIsNotNull() {
            addCriterion("cancletax is not null");
            return (Criteria) this;
        }

        public Criteria andCancletaxEqualTo(BigDecimal value) {
            addCriterion("cancletax =", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxNotEqualTo(BigDecimal value) {
            addCriterion("cancletax <>", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxGreaterThan(BigDecimal value) {
            addCriterion("cancletax >", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cancletax >=", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxLessThan(BigDecimal value) {
            addCriterion("cancletax <", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cancletax <=", value, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxIn(List<BigDecimal> values) {
            addCriterion("cancletax in", values, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxNotIn(List<BigDecimal> values) {
            addCriterion("cancletax not in", values, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cancletax between", value1, value2, "cancletax");
            return (Criteria) this;
        }

        public Criteria andCancletaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cancletax not between", value1, value2, "cancletax");
            return (Criteria) this;
        }

        public Criteria andTaxnameIsNull() {
            addCriterion("taxname is null");
            return (Criteria) this;
        }

        public Criteria andTaxnameIsNotNull() {
            addCriterion("taxname is not null");
            return (Criteria) this;
        }

        public Criteria andTaxnameEqualTo(String value) {
            addCriterion("taxname =", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameNotEqualTo(String value) {
            addCriterion("taxname <>", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameGreaterThan(String value) {
            addCriterion("taxname >", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameGreaterThanOrEqualTo(String value) {
            addCriterion("taxname >=", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameLessThan(String value) {
            addCriterion("taxname <", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameLessThanOrEqualTo(String value) {
            addCriterion("taxname <=", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameLike(String value) {
            addCriterion("taxname like", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameNotLike(String value) {
            addCriterion("taxname not like", value, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameIn(List<String> values) {
            addCriterion("taxname in", values, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameNotIn(List<String> values) {
            addCriterion("taxname not in", values, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameBetween(String value1, String value2) {
            addCriterion("taxname between", value1, value2, "taxname");
            return (Criteria) this;
        }

        public Criteria andTaxnameNotBetween(String value1, String value2) {
            addCriterion("taxname not between", value1, value2, "taxname");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxIsNull() {
            addCriterion("redpacketmoneytax is null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxIsNotNull() {
            addCriterion("redpacketmoneytax is not null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxEqualTo(BigDecimal value) {
            addCriterion("redpacketmoneytax =", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxNotEqualTo(BigDecimal value) {
            addCriterion("redpacketmoneytax <>", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxGreaterThan(BigDecimal value) {
            addCriterion("redpacketmoneytax >", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoneytax >=", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxLessThan(BigDecimal value) {
            addCriterion("redpacketmoneytax <", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoneytax <=", value, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxIn(List<BigDecimal> values) {
            addCriterion("redpacketmoneytax in", values, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxNotIn(List<BigDecimal> values) {
            addCriterion("redpacketmoneytax not in", values, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoneytax between", value1, value2, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneytaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoneytax not between", value1, value2, "redpacketmoneytax");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyIsNull() {
            addCriterion("cardtrademoney is null");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyIsNotNull() {
            addCriterion("cardtrademoney is not null");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyEqualTo(BigDecimal value) {
            addCriterion("cardtrademoney =", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyNotEqualTo(BigDecimal value) {
            addCriterion("cardtrademoney <>", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyGreaterThan(BigDecimal value) {
            addCriterion("cardtrademoney >", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cardtrademoney >=", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyLessThan(BigDecimal value) {
            addCriterion("cardtrademoney <", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cardtrademoney <=", value, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyIn(List<BigDecimal> values) {
            addCriterion("cardtrademoney in", values, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyNotIn(List<BigDecimal> values) {
            addCriterion("cardtrademoney not in", values, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cardtrademoney between", value1, value2, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andCardtrademoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cardtrademoney not between", value1, value2, "cardtrademoney");
            return (Criteria) this;
        }

        public Criteria andTforderIsNull() {
            addCriterion("tforder is null");
            return (Criteria) this;
        }

        public Criteria andTforderIsNotNull() {
            addCriterion("tforder is not null");
            return (Criteria) this;
        }

        public Criteria andTforderEqualTo(String value) {
            addCriterion("tforder =", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderNotEqualTo(String value) {
            addCriterion("tforder <>", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderGreaterThan(String value) {
            addCriterion("tforder >", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderGreaterThanOrEqualTo(String value) {
            addCriterion("tforder >=", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderLessThan(String value) {
            addCriterion("tforder <", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderLessThanOrEqualTo(String value) {
            addCriterion("tforder <=", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderLike(String value) {
            addCriterion("tforder like", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderNotLike(String value) {
            addCriterion("tforder not like", value, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderIn(List<String> values) {
            addCriterion("tforder in", values, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderNotIn(List<String> values) {
            addCriterion("tforder not in", values, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderBetween(String value1, String value2) {
            addCriterion("tforder between", value1, value2, "tforder");
            return (Criteria) this;
        }

        public Criteria andTforderNotBetween(String value1, String value2) {
            addCriterion("tforder not between", value1, value2, "tforder");
            return (Criteria) this;
        }

        public Criteria andServicemoneyIsNull() {
            addCriterion("servicemoney is null");
            return (Criteria) this;
        }

        public Criteria andServicemoneyIsNotNull() {
            addCriterion("servicemoney is not null");
            return (Criteria) this;
        }

        public Criteria andServicemoneyEqualTo(BigDecimal value) {
            addCriterion("servicemoney =", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyNotEqualTo(BigDecimal value) {
            addCriterion("servicemoney <>", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyGreaterThan(BigDecimal value) {
            addCriterion("servicemoney >", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("servicemoney >=", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyLessThan(BigDecimal value) {
            addCriterion("servicemoney <", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("servicemoney <=", value, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyIn(List<BigDecimal> values) {
            addCriterion("servicemoney in", values, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyNotIn(List<BigDecimal> values) {
            addCriterion("servicemoney not in", values, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("servicemoney between", value1, value2, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andServicemoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("servicemoney not between", value1, value2, "servicemoney");
            return (Criteria) this;
        }

        public Criteria andFenrunIsNull() {
            addCriterion("fenrun is null");
            return (Criteria) this;
        }

        public Criteria andFenrunIsNotNull() {
            addCriterion("fenrun is not null");
            return (Criteria) this;
        }

        public Criteria andFenrunEqualTo(BigDecimal value) {
            addCriterion("fenrun =", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunNotEqualTo(BigDecimal value) {
            addCriterion("fenrun <>", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunGreaterThan(BigDecimal value) {
            addCriterion("fenrun >", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fenrun >=", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunLessThan(BigDecimal value) {
            addCriterion("fenrun <", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fenrun <=", value, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunIn(List<BigDecimal> values) {
            addCriterion("fenrun in", values, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunNotIn(List<BigDecimal> values) {
            addCriterion("fenrun not in", values, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fenrun between", value1, value2, "fenrun");
            return (Criteria) this;
        }

        public Criteria andFenrunNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fenrun not between", value1, value2, "fenrun");
            return (Criteria) this;
        }

        public Criteria andMarkIsNull() {
            addCriterion("mark is null");
            return (Criteria) this;
        }

        public Criteria andMarkIsNotNull() {
            addCriterion("mark is not null");
            return (Criteria) this;
        }

        public Criteria andMarkEqualTo(String value) {
            addCriterion("mark =", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkNotEqualTo(String value) {
            addCriterion("mark <>", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkGreaterThan(String value) {
            addCriterion("mark >", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkGreaterThanOrEqualTo(String value) {
            addCriterion("mark >=", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkLessThan(String value) {
            addCriterion("mark <", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkLessThanOrEqualTo(String value) {
            addCriterion("mark <=", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkLike(String value) {
            addCriterion("mark like", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkNotLike(String value) {
            addCriterion("mark not like", value, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkIn(List<String> values) {
            addCriterion("mark in", values, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkNotIn(List<String> values) {
            addCriterion("mark not in", values, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkBetween(String value1, String value2) {
            addCriterion("mark between", value1, value2, "mark");
            return (Criteria) this;
        }

        public Criteria andMarkNotBetween(String value1, String value2) {
            addCriterion("mark not between", value1, value2, "mark");
            return (Criteria) this;
        }

        public Criteria andDateIsNull() {
            addCriterion("date is null");
            return (Criteria) this;
        }

        public Criteria andDateIsNotNull() {
            addCriterion("date is not null");
            return (Criteria) this;
        }

        public Criteria andDateEqualTo(String value) {
            addCriterion("date =", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotEqualTo(String value) {
            addCriterion("date <>", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThan(String value) {
            addCriterion("date >", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThanOrEqualTo(String value) {
            addCriterion("date >=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThan(String value) {
            addCriterion("date <", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThanOrEqualTo(String value) {
            addCriterion("date <=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLike(String value) {
            addCriterion("date like", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotLike(String value) {
            addCriterion("date not like", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateIn(List<String> values) {
            addCriterion("date in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotIn(List<String> values) {
            addCriterion("date not in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateBetween(String value1, String value2) {
            addCriterion("date between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotBetween(String value1, String value2) {
            addCriterion("date not between", value1, value2, "date");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}