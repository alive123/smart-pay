package com.founder.controller;

import com.alibaba.fastjson.JSON;
import com.founder.core.log.MyLog;
import com.founder.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@RestController
public class GatewayAli {

    private final static MyLog _log = MyLog.getLog(GatewayAli.class);

    @Autowired
    GatewayService gatewayService;

    @RequestMapping(value = "/pay/gateway")
    public Map<String, Object> gateway(String service, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", "100");
        result.put("msg", "未知错误");

        Enumeration<String> names = request.getParameterNames();
        _log.info("{}-----------------请求参数begin-----------------",service);
        String paramsStr = "";
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            paramsStr += name + " = " + request.getParameter(name) + "\n";
        }
        _log.info("{}-----------------参数:{}-----------------",service,paramsStr);
        _log.info("{}-----------------请求参数end-----------------",service);

        String clientIp = request.getParameter("client_ip");
        String hostName = request.getParameter("host_name");
        String channelId = "ALIPAY_QR";
        String mchId = request.getParameter("mch_id");
        _log.info("客户端计算机名：{},客户端IP地址：{},渠道ID：{},商户号：{}.",hostName,clientIp,channelId,mchId);

        try{
            if ("pp.trade.pay".equals(service)){
                _log.error("【支付宝】请求发生错误：未实现的交易。");
                result.put("code", "98");
                result.put("msg", "【支付宝】请求发生错误：未实现的交易。");
            } else if ("pp.trade.precreate".equals(service)) {
                request.setCharacterEncoding("GBK");
                String orderNo = request.getParameter("order_no");//订单号
                BigDecimal totalAmount = new BigDecimal(request.getParameter("total_amount"));
                String subject = request.getParameter("subject");//订单标题
                String storeId = request.getParameter("store_id");//商户门店编号
                String timeOutExpress = request.getParameter("timeout_express");//交易超时时间
                _log.info("【支付宝】金额：{}",totalAmount);

                result = gatewayService.precreate(mchId, channelId, orderNo, totalAmount, subject, storeId, timeOutExpress, clientIp, hostName);
            } else if ("pp.trade.refund".equals(service)) {
                String orderNo = request.getParameter("order_no");
                BigDecimal refundAmount = new BigDecimal(request.getParameter("refund_amount"));
                String refundReason = request.getParameter("refund_reason");
                _log.info("【支付宝】金额：{}",refundAmount);

                result = gatewayService.refund(orderNo, refundAmount, refundReason);
            } else if ("pp.trade.cancel".equals(service)) {
                String orderNo = request.getParameter("order_no");//订单号

                result = gatewayService.cancel(orderNo);
            } else if ("pp.trade.query".equals(service)) {
                String orderNo = request.getParameter("order_no");

                result = gatewayService.query(mchId, channelId, orderNo);
            } else {
                _log.error("【支付宝】请求发生错误：未知的交易。");
                result.put("code", "97");
                result.put("msg", "【支付宝】请求发生错误：未知的交易。");
            }
        } catch (Exception e) {
            _log.error(e,"【支付宝】请求发生异常：" + e.getMessage());
            result.put("code", "99");
            result.put("msg", "【支付宝】请求处理发生异常");
        }

        _log.info("【支付宝】服务请求返回：" + JSON.toJSONString(result));

        return result;
    }
}
