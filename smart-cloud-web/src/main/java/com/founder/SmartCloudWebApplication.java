package com.founder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SmartCloudWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartCloudWebApplication.class, args);
    }
}
